# Jenkins pipelines description for `food-camp` project

This is a Jenkins pipeline repo for food-camp project

## Build image pipeline

This pipeline is used to publish new version of docker image to *dockerhub* repo

Steps:

- Checkout
    Here pipeline checkouts to the latest commit in site repo and downloads latest version of code
- Login
    Step to login to Dockerhub account via cli
- Build
    Use command `docker image build` to create new version of image 
- Test
    Sample run of new docker contianer with latest image to ensure its running
- Push
    Push new changes to Dockerhub
- Cleanup
    Stop all running containers and remove cached and created images

## Deploy infrastructure pipeline

This pipeline is used to deploy new Terraform-based resources to cloud

Steps:

- Checkout
    Here pipeline checkouts to the latest commit in site repo and downloads latest version of code
- Initialize
    `terraform init` to initialize Terraform providers and modules
- Plan
    Create a plan for resources deployment using `terraform plan`
- Apply
    Apply created plan and deploy resources

## Deploy TEMP site

Pipeline to create new deployment to temp host in GCP from *temp* branch

Steps:

- Checkout
    Here pipeline checkouts to the latest commit in site repo and downloads latest version of code
- Login
    This step is used to login to *Dockerhub* account to download latest image version
- Test
    Test new image and *docker-compose* locally before deploying to remote host
- Deploy
    Finally deploy new code from *temp* branch to remote host via ssh

## Author

Krazhevskiy Aleksey

- [github](https://github.com/alekseykrazhev)
- [gitlab](https://gitlab.com/alekseykrazhev)
- [dockerhub](https://hub.docker.com/u/alekseykrazhev)
